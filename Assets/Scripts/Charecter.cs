﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charecter : MonoBehaviour {
	public float rotationSpeed;
	public float startingSpeed;
  public GameObject canvas;

	private bool playGame = false;
  [SerializeField]
	private GameObject[] barriers;
	[SerializeField]
	private Vector3 lastBarrier;
	private float currentSpeed;

	void Start () {
		currentSpeed = startingSpeed;
		SpawnBarrier();
	}

	// Update is called once per frame
	void Update () {
		if (playGame) {
			Vector2 rotTarget = this.transform.position;
			if (Input.touchCount > 0) {
				rotTarget = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
				rotTarget.y += 3;
			} else {
				rotTarget.y += 3;
			}
			Vector2 direction = rotTarget - (Vector2)this.transform.position;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;
			Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
			this.transform.Translate(Vector2.up * currentSpeed * Time.deltaTime);
		} else {
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began || Input.GetKeyDown("space")) {
				Begin();
			}
		}
	}
	void OnCollisionEnter2D(Collision2D obj) {
		if (obj.collider.transform.tag == "Goal") {
			Goal();
			return;
		}
		if (obj.collider.transform.tag == "Death") {
			Death();
			return;
		}
	}
	void Begin() {
		playGame = true;
		canvas.SetActive(false);
		// hs
	}
	void Death() {
		playGame = false;
		canvas.SetActive(true);
  	Camera.main.gameObject.GetComponent<CameraShake>().shakeCamera();
		// spit particles
		// update hs
		this.transform.position = Vector2.Lerp(this.transform.position, Vector2.zero, currentSpeed * 2);
		this.transform.rotation = Quaternion.identity;
		currentSpeed = startingSpeed;
	}
  void Goal() {
		currentSpeed = currentSpeed + (currentSpeed * 0.02f);
		SpawnBarrier();
	}
	void SpawnBarrier() {
		lastBarrier.y += 5;
		Instantiate(barriers[Random.Range(0,barriers.Length)], lastBarrier, Quaternion.identity);
	}
}
