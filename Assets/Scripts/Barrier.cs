﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrier : MonoBehaviour {
  public int chance = 70;
	public GameObject coin;

	void Start () {
		if (coin) {
			int choice = Random.Range(0,100);
			if (choice > chance) {
				Vector2 coinPos = this.transform.position;
				coinPos.y += 2.5f;
				Instantiate(coin, coinPos, Quaternion.identity);
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (this.transform.position.y < (Camera.main.gameObject.transform.position.y - 8)) {
			Destroy(this.gameObject);
		}
	}
}
